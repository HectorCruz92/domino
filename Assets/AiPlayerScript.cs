﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AiPlayerScript : MonoBehaviour {


    public GameState gameState;
    public Player playerHand;
    public bool ItsMyTurn;
    public fieldPlayer fieldScript;
    public FieldData fieldData;
    public GameObject fieldPanel;
    public Vector3 rotation;
    public Vector3 pos;
    public Vector3 scale;

    // Use this for initialization
    private bool size = false;

    void Start()
    {
        
    }

    public void createHand()
    {
        var rt = gameObject.GetComponent<RectTransform>();

        
        rt.rotation = Quaternion.Euler(new Vector3(0,0,0));
        rt.localScale = new Vector3(1,1,1);

        GetComponent<TileCreator>().CreateTiles();
       
        rt.localPosition = pos;
        rt.rotation = Quaternion.Euler(rotation);
        rt.localScale = scale;
    }
    
    IEnumerator Example()
    {
        
        yield return new WaitForSeconds(1);
        AiPlayFirstAvailable();
        ItsMyTurn = false;

    }
    public void MyTurn()
    {
        if (!gameState.GameOver)
        {
            ItsMyTurn = true;
            StartCoroutine(Example());
            
        }
        
        
    }
    
    private void AiPlayFirstAvailable()
    {
        var p = transform.GetComponentsInChildren<handLayoutTile>();
        for (int i = 0; i < transform.childCount; i++)
        {
            var topNumber = p[i].top;
            var botNumber = p[i].bot;
            var child = p[i];
            if (gameState.numberOfGamesPlayed == 0&& fieldData.moveCounter == 0)
            {
                for (int k = 0; k < transform.childCount; k++)
                {
                    if (p[k].top==6&&p[k].bot ==6)
                    {
                        p[k].transform.SetParent(fieldPanel.transform);
                        fieldScript.playTopTest(p[k].player, p[k].domino);
                        p[k].PlayOnField();
                        return;
                    }
                }
            }
            if (fieldData.moveCounter == 0)
            {
            
                child.transform.SetParent(fieldPanel.transform);
                fieldScript.playTopTest(p[i].player, p[i].domino);
                child.PlayOnField();
                return;
            }
            else if (topNumber == fieldData.topNumber || botNumber == fieldData.topNumber)
            {
                child.transform.SetParent(fieldPanel.transform);
                fieldScript.playTopTest(p[i].player, p[i].domino);
                child.PlayOnField();
                return;
            }
            else if (topNumber == fieldData.botNumber || botNumber == fieldData.botNumber)
            {
                child.transform.SetParent(fieldPanel.transform);
                fieldScript.playBotTest(p[i].player, p[i].domino);
                child.PlayOnField();
                return;
            }
            

        }
        playerHand.skip = true;
        gameState.NextTurn();

    }
    IEnumerator waitTest()
    {
        
        yield return new WaitForSeconds(2);
        var rt = GetComponent<RectTransform>();
        rt.localScale = scale;

    }

    public void ChangeSize()
    {
        var rt = GetComponent<RectTransform>();
        rt.localScale = new Vector3(1F, 1f, 1f);
        StartCoroutine( waitTest());
    }
    public void placehg()
    {
        var rt = GetComponent<RectTransform>();
        if (size)
        {
            rt.localScale = scale;

        }
        else
        {
            rt.localScale = new Vector3(1.1F, 1.1f, 1.1f);
        }
        size = !size;
    }
}
