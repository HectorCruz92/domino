﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Dragable : MonoBehaviour, IBeginDragHandler,IDragHandler, IEndDragHandler{

    public Transform parentToReturnTo = null;
    GameObject placeholder = null;
    public Transform placeholderParent = null;
    public bool onField = false;
    

    public void Start()
    {
        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {

        
        placeholder = new GameObject();
        placeholder.transform.SetParent(this.transform.parent);
        RectTransform rt = placeholder.AddComponent<RectTransform>();
        rt.sizeDelta = new Vector2(this.GetComponent<LayoutElement>().preferredWidth, 
                                                                            this.GetComponent<LayoutElement>().preferredHeight);
    
        LayoutElement le = placeholder.AddComponent<LayoutElement>();
        le.preferredWidth = this.GetComponent<LayoutElement>().preferredWidth;
        le.preferredHeight = this.GetComponent<LayoutElement>().preferredHeight;
        le.flexibleWidth = 0;
        le.flexibleHeight = 0;
        
        placeholder.transform.SetSiblingIndex(this.transform.GetSiblingIndex());
       
        parentToReturnTo = this.transform.parent;
        placeholderParent = parentToReturnTo;
        this.transform.SetParent(this.transform.parent.parent);

        GetComponent<CanvasGroup>().blocksRaycasts = false;
        
    }
    public void OnDrag(PointerEventData eventData)
    {

        this.transform.position = eventData.position;

        if (placeholder.transform.parent!= placeholderParent)
        {
            placeholder.transform.SetParent(placeholderParent);
        }
        int newSiblingIndex = placeholderParent.childCount;
        for (int i = 0; i < placeholderParent.childCount; i++)
        {
            if (this.transform.position.x < placeholderParent.GetChild(i).position.x)
            {
                newSiblingIndex = i;
                if (placeholder.transform.GetSiblingIndex()< newSiblingIndex)
                {
                    newSiblingIndex--;
                }
                
                break;
            }
        }
        placeholder.transform.SetSiblingIndex(newSiblingIndex);
    }
    public void OnEndDrag(PointerEventData eventData)
    {
       
        this.transform.SetParent(parentToReturnTo);
        this.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
        
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        Destroy(placeholder);

        if (onField)
        {
            GetComponent<handLayoutTile>().PlayOnField();
            this.enabled = false;
            
        }

        //GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
    }
    
        // Use this for initialization
   
    
    // Update is called once per frame
    void Update () {
		
	}
}
