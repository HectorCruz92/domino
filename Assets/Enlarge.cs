﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enlarge : MonoBehaviour {

    private Vector3 original;
    private bool size = false;
    private void Start()
    {
        original = GetComponent<AiPlayerScript>().scale;     
    }
    public void OnMouseEnter()
    {
        original = transform.localScale;
        Debug.Log("enter");
        transform.localScale += new Vector3(1.1F, 1.1f, 1.1f); //adjust these values as you see fit
    }
    public void OnMouseExit()
    {
        Debug.Log("left");
        transform.localScale = original;  // assuming you want it to return to its original size when your mouse leaves it.
    }
    public void ChangeSize()
    {
        var rt = GetComponent<RectTransform>();
        if (size)
        {
            rt.localScale = original;

        }
        else
        {
            rt.localScale = new Vector3(1.1F, 1.1f, 1.1f);
        }
        size = !size;
    }

    
}
