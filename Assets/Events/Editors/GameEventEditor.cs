﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GameEventEditor : Editor {

    // Use this for initialization
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUI.enabled = Application.isPlaying;
        GameEvent e = target as GameEvent;
        if (GUILayout.Button("Raise"))
        {
            e.Raise();
        }
    }
}
