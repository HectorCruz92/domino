﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameState : MonoBehaviour {


    public List<Domino> control;
    public List<Player> players;
    public GameEvent dealtHand;
    public FieldData fieldData;
    public fieldPlayer fieldScript;
    public List<GameEvent> turns;
    public int numberOfGamesPlayed;
    public int playerTurn;
    [Space]
    public bool GameOver;
    public int scoreGoal;

    public GameObject Menu;
    public Text player2;
    public Text player3;
    public Text player4;
    public Text showWinner;
    [Space]
    public List<float> pointsInHand;
    public float PointsLeft;
    bool menuOn;
    public GameObject nextGameBtn;

    [Space]
    public List<int> pointsForPlayers;
    public GameObject pointsPanel;


    int lastWinner;

    private void Start()
    {
        PointsLeft = 168f;
        GameOver = false;
        menuOn = false;
        Menu.SetActive(false);
        createHand();
        pointsForPlayers = new List<int>(new int[4]);
        nextGameBtn.SetActive(false);

        //nextGameBtn.SetActive(false);

        checkWhoGoesFirst();

    }
    private void checkWhoGoesFirst()
    {
        //la primera mano sale 6 6
        if (numberOfGamesPlayed == 0)
        {
            
            foreach (var domino in players[0].hand)
            {
                if (domino.value.x==6f&&domino.value.y==6f)
                {
                    turns[0].Raise();
                    playerTurn = 0;
                    return;
                }
            }
            foreach (var domino in players[1].hand)
            {
                if (domino.value.x == 6f && domino.value.y == 6f)
                {
                    turns[1].Raise();
                    playerTurn = 1;
                    return;
                }
            }
            foreach (var domino in players[2].hand)
            {
                if (domino.value.x == 6f && domino.value.y == 6f)
                {
                    turns[2].Raise();
                    playerTurn = 2;
                    return;
                }
            }
            foreach (var domino in players[3].hand)
            {
                if (domino.value.x == 6f && domino.value.y == 6f)
                {
                    turns[3].Raise();
                    playerTurn = 3;
                    return;
                }
            }

        }
        else
        {
            turns[lastWinner].Raise();
        }
        
    }
    private void Update()
    {
    }

    private void ChangePointsPanelText()
    {
        var go = pointsPanel.GetComponentsInChildren<Text>();
        for (int i = 0; i < go.Length; i++)
        {
            var p = pointsForPlayers[i].ToString();
            go[i].text =players[i].name+": "+p  +" Points";
        }
        
    }

    private void WinConditionLockedGame()
    {
        if (players[0].skip && players[1].skip && players[2].skip && players[3].skip)
        {
            var fullPoints = 0;
            foreach (Player item in players)
            {
                fullPoints += item.points;
            }
            GameOver = true;

            Player locker = fieldData.player[fieldData.player.Count - 1];
            //Debug.Log(playerTurn);
            var lockerString = locker.name.ToString();

            var w = playerTurn;
            var l = playerTurn + 1;
            if (l > 3)
            {
                l = 0;
            }
            if (players[w].points <= players[l].points)
            {
                fullPoints -= players[w].points;

                pointsForPlayers[w] += fullPoints;
                showWinner.text = "Game Was Lock by " + lockerString+" and he won ";
                lastWinner = w;
            }
            else
            {
                fullPoints -= players[l].points;
                pointsForPlayers[l] += fullPoints;
                showWinner.text = "Game Was Lock by " + lockerString + " and he lost ";
                lastWinner = w+1;
            }
            OpenMenu();
            nextGameBtn.SetActive(true);


        }
        

    }

    private void WinConditionOutOfHand()
    {
        var k = 0;
        if (players[1].hand.Count == 0)
        {
            GameOver = true;
            nextGameBtn.SetActive(true);
            k = players[0].points + players[2].points + players[3].points;
            pointsForPlayers[1] += k;
            showWinner.text = "Player 2 Won the Game";
            OpenMenu();
            lastWinner = 1;
        }
        else if (players[2].hand.Count == 0)
        {
            GameOver = true;
            nextGameBtn.SetActive(true);
            k = players[0].points + players[1].points + players[3].points;
            pointsForPlayers[2] += k;
            showWinner.text = "Player 3 Won the Game";
            OpenMenu();
            lastWinner = 2;
        }
        else if (players[3].hand.Count == 0)
        {
            GameOver = true;
            nextGameBtn.SetActive(true);
            k = players[0].points + players[1].points + players[2].points;
            pointsForPlayers[3] += k;
            showWinner.text = "Player 4 Won the Game";
            OpenMenu();
            lastWinner = 3;
        }
        else if (players[0].hand.Count == 0)
        {

            GameOver = true;
            nextGameBtn.SetActive(true);
            k = players[1].points + players[2].points + players[3].points;
            pointsForPlayers[0] += k;
            OpenMenu();
            showWinner.text = "You Won the Game!!";
            lastWinner = 0;
        }
    }


    private void CounterForOtherPlayersHands()
    {
        player2.text = "Player2: " + players[1].hand.Count;
        player3.text = "Player3: " + players[2].hand.Count;
        player4.text = "Player4: " + players[3].hand.Count;
    }

    
    public void OpenMenu()
    {
        menuOn = !menuOn;
        Menu.SetActive(menuOn);
    }
    public void Shuffle()
    {
        for (int i = 0; i < control.Count; i++)
        {
            //Holds temporary copy of value in spot X
            Domino temp = control[i];


            //creates random value
            int randomIndex = UnityEngine.Random.Range(0, control.Count);

            //asign value of spot X in Spot Y
            control[i] = control[randomIndex];

            //asign value of spot Y in Spot X 
            control[randomIndex] = temp;

        }
    }
    public void createHand()

    {
        // Shiffle control
        Shuffle();

        //Reset all player hands
        foreach (Player player in players)
        {
            player.Reset();
            
        }

        //resetFieldData
        fieldData.ResetField();

        // give 7 cards from control to each player
        for (int i = 0; i < 7; i++)
        {
            players[0].AddDomino(control[i]);
        }
        for (int i = 7; i < 14; i++)
        {
            players[1].AddDomino(control[i]);
        }
        for (int i = 14; i < 21; i++)
        {
            players[2].AddDomino(control[i]);
        }
        for (int i = 21; i < 28; i++)
        {
            players[3].AddDomino(control[i]);
        }
        dealtHand.Raise();
    }

    public void NextGame()
    {
        nextGameBtn.SetActive(false);
        OpenMenu();
        fieldScript.ResetBoard();
        numberOfGamesPlayed++;
        createHand();
        GameOver = false;
        checkWhoGoesFirst();
        //nextGameBtn.SetActive(false);
    }
    public void NextTurn()
    {
        CounterForOtherPlayersHands();
        WinConditionOutOfHand();
        WinConditionLockedGame();
        ChangePointsPanelText();
        ScoreGoalReached();

        if (!GameOver)
        {
            playerTurn++;
            if (playerTurn > 3)
            {
                playerTurn = 0;
            }
            
            turns[playerTurn].Raise();
        }
        
        
    }

    private void ScoreGoalReached()
    {
        for (int i = 0; i < pointsForPlayers.Count; i++)
        {
            if (pointsForPlayers[i] == scoreGoal)
            {
                GameOver = true;
                showWinner.text = players[i].name+ " got to "+scoreGoal+" first";
                OpenMenu();
                nextGameBtn.SetActive(false);
            }
        }
    }

    public void testWinner()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            var k = 0;
            GameOver = true;
            k = players[1].points + players[2].points + players[3].points;
            pointsForPlayers[0] += k;
            OpenMenu();
            showWinner.text = "You Won the Game!!";
            lastWinner = 0;
        }
        if (Input.GetKey(KeyCode.Space))
        {
            GameOver = true;
            OpenMenu();
            Player locker = fieldData.player[fieldData.player.Count - 1];
            Debug.Log(playerTurn);
            var lockerString = locker.name.ToString();
            showWinner.text = "Game Was Lock by " + lockerString;
        }


        
    }
}
