﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuResetScript : MonoBehaviour {

    public Player control;
    public List<Player> players;
    public FieldData fieldData;
    // Use this for initialization
    public void Shuffle()
    {
        for (int i = 0; i < control.hand.Count; i++)
        {
            //Holds temporary copy of value in spot X
            Domino temp = control.hand[i];


            //creates random value
            int randomIndex = UnityEngine.Random.Range(0, control.hand.Count);

            //asign value of spot X in Spot Y
            control.hand[i] = control.hand[randomIndex];

            //asign value of spot Y in Spot X 
            control.hand[randomIndex] = temp;

        }
    }
    public void createHand()

    {
        // Shiffle player.hand
        Shuffle();

        //Reset all player hands
        foreach (Player player in players)
        {
            player.Reset();

        }

        //resetFieldData
        fieldData.ResetField();

        // give 7 cards from player.hand to each player
        for (int i = 0; i < 7; i++)
        {
            players[0].hand.Add(control.hand[i]);
        }
        for (int i = 7; i < 14; i++)
        {
            players[1].hand.Add(control.hand[i]);
        }
        for (int i = 14; i < 21; i++)
        {
            players[2].hand.Add(control.hand[i]);
        }
        for (int i = 21; i < 28; i++)
        {
            players[3].hand.Add(control.hand[i]);
        }
        
    }

}
