﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLogic : MonoBehaviour {

    public FieldData fieldData;
    public Player player;
    public GameState gameState;


    public void Skip()
    {
        if (gameState.playerTurn==0)
        {
            player.skip = true;
            gameState.NextTurn();
        }
       
    }
	// Use this for initialization
	void Start () {
		

	}
	public void checkIfCanPlay()
    {
        for (int i = 0; i < player.hand.Count; i++)
        {
            var top = Mathf.RoundToInt(player.hand[i].value.x);
            var bot = Mathf.RoundToInt(player.hand[i].value.y);

            if (fieldData.moveCounter==0||
                top == fieldData.topNumber||
                top == fieldData.botNumber||
                bot == fieldData.topNumber||
                bot == fieldData.botNumber)
            {

            }
            else
            {
                Skip();
            }
        }
        
    }
	// Update is called once per frame
	void Update () {
		
	}
}
