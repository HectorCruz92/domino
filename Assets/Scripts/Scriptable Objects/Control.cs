﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="GameStateController", menuName ="GameStateController")]
public class GameStateObject : ScriptableObject {
    public List<Domino> control;
    public List<Player> players;
    [Space]
    public FieldData fieldData;
    [Space]
    public GameEvent dealtHand;
    




    public void Shuffle()
    {
        for (int i = 0; i < control.Count; i++)
        {
            //Holds temporary copy of value in spot X
            Domino temp = control[i];
           

            //creates random value
            int randomIndex = UnityEngine.Random.Range(0, control.Count);

            //asign value of spot X in Spot Y
            control[i] = control[randomIndex];
           
            //asign value of spot Y in Spot X 
            control[randomIndex] = temp;
            
        }
    }
    public void createHand()
   
    {
        // Shiffle control
        Shuffle();

        //Reset all player hands
        foreach (Player player in players)
        {
            player.Reset();
            
        }

        //resetFieldData
        fieldData.ResetField();

        // give 7 cards from control to each player
        for (int i = 0; i < 7; i++)
        {
            players[0].hand.Add(control[i]);
        }
        for (int i = 7; i < 14; i++)
        {
            players[1].hand.Add(control[i]);
        }
        for (int i = 14; i < 21; i++)
        {
            players[2].hand.Add(control[i]);
        }
        for (int i = 21; i < 28; i++)
        {
            players[3].hand.Add(control[i]);
        }
        dealtHand.Raise();
    }

}
