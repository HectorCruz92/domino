﻿
using UnityEngine;

[CreateAssetMenu(fileName="DominoTile",menuName ="DominoTile")]
public class Domino : ScriptableObject {

    public Vector2 value;
    public string domino;
    public Sprite artwork;
    public bool isDouble;
    
	
}
