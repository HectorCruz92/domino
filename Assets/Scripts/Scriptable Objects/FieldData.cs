﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="FieldData",menuName ="FieldData")]
public class FieldData : ScriptableObject {

    public List<Player> player;
    public List<Domino> domino;
    public List<Vector2> position;
    public List<Vector3> rotation;
    public int topNumber=-10;
    public int botNumber=-10;
    public int moveCounter;
    public Vector2 topPos;
    public Vector2 botPos;



    public void ResetField()
    {
        domino.Clear();
        position.Clear();
        player.Clear();
        rotation.Clear();
        topNumber = -10;
        botNumber = -10;
        moveCounter = 0;
        topPos = new Vector2(0, 0);
        botPos = new Vector2(0, 0);

    }
    public void addDominoTile(Player playerNumber, Domino dom, Vector2 pos, Vector3 rot)
    {
        player.Add(playerNumber);
        domino.Add(dom);
        position.Add(pos);
        rotation.Add(rot);
        

    }
    public void addToTop(int top)
    {
        topNumber = top;
    }
    public void addToBot(int bot)
    {
        botNumber = bot;
    }






}
