﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="gameSetting", menuName ="Settings/GameSettings")]
public class GameSettings : ScriptableObject {


    public List<TileArtWork> frontArtwork;
    public List<Domino> Control;
	
}
