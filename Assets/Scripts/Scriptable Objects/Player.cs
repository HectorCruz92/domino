﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="PlayerData", menuName ="PlayerHolder")]
public class Player : ScriptableObject {

    public List<Domino> hand;
    public int points;
    public bool skip;

    public void Reset()
    {
        hand.Clear();
        points = 0;
        skip = false;
    }
    public void removeFromHand(Domino domino)
    {
        hand.Remove(domino);
        points = points - Mathf.RoundToInt(domino.value.x + domino.value.y);
        skip = false;

    }
    public void AddDomino(Domino domino)
    {
        hand.Add(domino);
        points = points + Mathf.RoundToInt(domino.value.x + domino.value.y);
    }
}
