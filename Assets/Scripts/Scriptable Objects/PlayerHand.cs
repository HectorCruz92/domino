﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="PlayerHand",menuName ="Player")]
public class PlayerHand : ScriptableObject {
    
    public List<string> hand;
    public List<Sprite> artwork;
    public List<Vector2> values;
    

   
    public void reset()
    {
        hand.Clear();
        artwork.Clear();
        values.Clear();

    }
    public void Shuffle()
    {
        for (int i = 0; i < hand.Count; i++)
        {
            //Holds temporary copy of value in spot X
            string temp = hand[i];
            Sprite temp2 = artwork[i];
            Vector2 temp3 = values[i];

            //creates random value
            int randomIndex = UnityEngine.Random.Range(0, hand.Count);

            //asign value of spot X in Spot Y
            hand[i] = hand[randomIndex];
            artwork[i] = artwork[randomIndex];
            values[i] = values[randomIndex];

            //asign value of spot Y in Spot X 
            hand[randomIndex] = temp;
            artwork[randomIndex] = temp2;
            values[randomIndex] = temp3;
        }
    }
}
