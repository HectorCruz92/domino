﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="FrontTileArtwork",menuName ="art/FrontTile")]
public class TileArtWork : ScriptableObject {
    public List<Sprite> artwork;
	

}
