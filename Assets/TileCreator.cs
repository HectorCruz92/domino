﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileCreator : MonoBehaviour {

    public GameState gameState;
    public Player player;
    public GameObject prefab;
    public fieldPlayer fieldScript;
    
    public bool PlayerControl;
    
    
    // Use this for initialization
    void Start () {
        
        
    }
    // Update is called once per frame
    void Update () {
        
	}
    public void CreateTiles()
    {
        
        for (int i = 0; i < player.hand.Count; i++)
        {
            //var newPos = firstPos + new Vector3(widthForHand * i, bottomPos, 0);
            var _tile= Instantiate(prefab);
            var tileScript = _tile.GetComponent<handLayoutTile>();
            _tile.GetComponent<Dragable>().enabled = PlayerControl;
            if (!PlayerControl)
            {
                tileScript.ai = true;
            }
            tileScript.domino = player.hand[i];
            tileScript.player = player;
            tileScript.fieldScript= fieldScript;
            _tile.transform.SetParent(gameObject.transform);
        }
        
    }
    
}
