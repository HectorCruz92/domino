﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class fieldPlayer : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{

    public FieldData fieldData;
    public GameState gameState;
    public GameEvent playedDomino;
    [Space]
    public Vector2 posTop;
    public Vector2 posBot;
    public float shift;
    float limitX;
    float limitY;
    bool isDouble;
    bool isValid;
    bool pastTopDouble;
    bool pastBotDouble;
    int topTurnCounter;

    int botTurnCounter;



    
    [Space]
    bool pastDouble;
    GameObject newDomino;
    Vector2 pos;
    public Vector3 rotation;
    Vector2 mouseDrop;

    
    float height;
    // Use this for initialization
    void Start()
    {
        posTop = new Vector2(0, 0);
        posBot = new Vector2(0, 0);
        pastTopDouble = false;
        pastBotDouble = false;


        height = 0.0625f * Screen.height;
        shift =height/4 ;
        limitY = (Screen.height * (0.9f - 0.173f) / 2) - (shift * 4);
        limitX = ((Screen.width/2) - (shift * 4));
        isDouble = false;

        pos = new Vector2(0, 0);
        topTurnCounter = 0;
        botTurnCounter = 0;
        fieldData.ResetField();
      
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
        {
            return;
        }
        Dragable d = eventData.pointerDrag.GetComponent<Dragable>();
        handLayoutTile t = eventData.pointerDrag.GetComponent<handLayoutTile>();
        if (d != null)
        {
            checkIfValid(t);
            if (isValid)
            {
                d.parentToReturnTo = this.transform;
            }
            
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
        {
            return;
        }
        Dragable d = eventData.pointerDrag.GetComponent<Dragable>();
        if (d != null && d.placeholderParent == this.transform)
        {
            
            if (isValid)
            {
                d.parentToReturnTo = this.transform;
            }
           


        }
    }
    public void OnDrop(PointerEventData eventData)
    {
        Dragable d = eventData.pointerDrag.GetComponent<Dragable>();
        handLayoutTile t = eventData.pointerDrag.GetComponent<handLayoutTile>();
        mouseDrop = eventData.pointerDrag.GetComponent<RectTransform>().position - gameObject.GetComponent<RectTransform>().position;
        
        if (d != null)
        {
            
            if (isValid)
            {

                d.parentToReturnTo = this.transform;
                PlayPosition(t);
                
                d.onField = true;
            }
          
           
        }
    }

    public void PlayPosition(handLayoutTile t)
    {
        if (fieldData.moveCounter==0)
        {
            playTopTest(t.player, t.domino);
        }
        else if (fieldData.topNumber == t.top || fieldData.topNumber == t.bot)
        {
            if (fieldData.botNumber == t.top || fieldData.botNumber == t.bot)
            {
                var disTop = Vector2.Distance(mouseDrop, posTop);
                var disBot = Vector2.Distance(mouseDrop, posBot);
                
                if (disTop < disBot)
                {
                    playTopTest(t.player, t.domino);
                }
                else
                {
                    playBotTest(t.player, t.domino);
                }
            }
            else
            {
                playTopTest(t.player, t.domino);
            }
           
        }
        else if (fieldData.botNumber == t.top || fieldData.botNumber == t.bot)
        {
            playBotTest(t.player, t.domino);
        }
        
    }
    public void checkIfValid(handLayoutTile t)
    {
        if (t.player == gameState.players[gameState.playerTurn])
        {
            if (fieldData.topNumber == t.top || fieldData.topNumber == t.bot ||
                        fieldData.botNumber == t.top || fieldData.botNumber == t.bot)
            {
                isValid = true;
            }
            else
            {
                isValid = false;
            }
            if (fieldData.moveCounter == 0)
            {
                isValid = true;
            }
        }
        else
        {
            isValid = false;
        }
        
          
    }

    
   
    public void playTopTest(Player player, Domino domino)
    {
        rotation = new Vector3(0, 0, 0);
        isDouble = domino.isDouble;

        //change below for bot
        pos = posTop;
        pastDouble = pastTopDouble;
        //^^^^^^^^^^^
        if (Mathf.RoundToInt(domino.value.x) == fieldData.topNumber)
        {
            rotation = rotation + new Vector3(0, 0, 180);
            fieldData.topNumber = Mathf.RoundToInt(domino.value.y);
        }
        else
        {
            fieldData.topNumber = Mathf.RoundToInt(domino.value.x);
        }

        if (fieldData.moveCounter == 0)
        {
            if (isDouble)
            {
                rotation = new Vector3(0, 0, 90);

            }
            fieldData.topNumber = Mathf.RoundToInt(domino.value.x);
            fieldData.botNumber = Mathf.RoundToInt(domino.value.y);
            fieldData.moveCounter++;
            pastBotDouble = isDouble;

        }
        else if (topTurnCounter == 0)
        {

            if (pos.y + 4 * shift > limitY)
            {
                turnLeft();
                MoveLeft();
                topTurnCounter++;

            }
            else
            {
                MoveUp();
            }
        }
        else if (topTurnCounter == 1)
        {
            if (pos.x - 4 * shift < -limitX)
            {
                rotation = rotation + new Vector3(0, 0, 180);
                turnDown();
                MoveDown();
                topTurnCounter++;

            }
            else
            {
                MoveLeft();
            }
        }
        else if (topTurnCounter == 2)
        {
            if (pos.y - 4 * shift < -limitY)
            {
                turnRight();
                MoveRight();
                topTurnCounter++;

            }
            else
            {
                rotation = rotation + new Vector3(0, 0, 180);
                MoveDown();
            }
        }
        else if (topTurnCounter == 3)
        {
            if (pos.x + 4 * shift > -shift*6)
            {
                turnUp();
                MoveUp();
                topTurnCounter++;

            }
            else
            {
                MoveRight();
            }
        }
        else if (topTurnCounter > 3)
        {
            MoveUp();
        }
        if (rotation.z > 360)
        {
            rotation.z = rotation.z - 360;
        }
        fieldData.addDominoTile(player, domino, pos, rotation);
        //GameObject newDomino = Instantiate(prefab, pos, Quaternion.identity) as GameObject;

        //newDomino.transform.position = pos;
        //newDomino.transform.Rotate(rotation);
        //newDomino.GetComponent<SpriteRenderer>().sprite = domino.artwork;
        
        //change below for bot
        posTop = pos;

        if (isDouble)
        {
            pastTopDouble = true;
        }
        else
        {
            pastTopDouble = false;
        }
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        playedDomino.Raise();


    }
    public void playBotTest(Player player, Domino domino)
    {
        rotation = new Vector3(0, 0, 0);
        isDouble = domino.isDouble;

        //change below for bot
        pos = posBot;
        pastDouble = pastBotDouble;
        //^^^^^^^^^^^
        if (Mathf.RoundToInt(domino.value.x) == fieldData.botNumber)
        {

            fieldData.botNumber = Mathf.RoundToInt(domino.value.y);
        }
        else
        {
            rotation = rotation + new Vector3(0, 0, 180);
            fieldData.botNumber = Mathf.RoundToInt(domino.value.x);
        }

        if (fieldData.moveCounter == 0)
        {
            if (isDouble)
            {
                rotation = new Vector3(0, 0, 90);
            }
            fieldData.moveCounter++;
            pastTopDouble = isDouble;
        }
        else if (botTurnCounter == 0)
        {

            if (pos.y - 4 * shift < -limitY)
            {
                turnRight();
                MoveRight();
                botTurnCounter++;

            }
            else
            {
                MoveDown();
            }
        }
        else if (botTurnCounter == 1)
        {
            if (pos.x + 4 * shift > limitX)
            {
                rotation = rotation + new Vector3(0, 0, 180);
                turnUp();
                MoveUp();
                botTurnCounter++;

            }
            else
            {
                MoveRight();
            }
        }
        else if (botTurnCounter == 2)
        {
            if (pos.y + 4 * shift > limitY)
            {
                turnLeft();
                MoveLeft();
                botTurnCounter++;

            }
            else
            {
                rotation = rotation + new Vector3(0, 0, 180);
                MoveUp();
            }
        }
        else if (botTurnCounter == 3)
        {
            if (pos.x - 4 * shift < shift*6.5)
            {
                turnDown();
                MoveDown();
                botTurnCounter++;

            }
            else
            {
                MoveLeft();
            }
        }
        else if (botTurnCounter > 3)
        {
            MoveDown();
        }

        //change below for bot
        if (rotation.z >360)
        {
            rotation.z = rotation.z - 360;
        }

        fieldData.addDominoTile(player, domino, pos, rotation);
        posBot = pos;

        if (isDouble)
        {
            pastBotDouble = true;
        }
        else
        {
            pastBotDouble = false;
        }
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        playedDomino.Raise();
    }


    


    // Update is called once per frame
    void Update()
    {
       
    }

    


    


    public void ResetBoard()
    {
        var allChildren = GameObject.FindGameObjectsWithTag("DominoTile");
        foreach (GameObject obj in allChildren)
        {

            Destroy(obj);

        }
        
        posTop = new Vector2(0, 0);
        posBot = new Vector2(0, 0);
        pastTopDouble = false;
        pastBotDouble = false;

        topTurnCounter = 0;
        botTurnCounter = 0;
        isDouble = false;

        pos = new Vector2(0, 0);


    }

    public void MoveUp()
    {
        // GameObject newDomino = Instantiate(prefab, new Vector2(0, 0), Quaternion.identity) as GameObject;

        if (isDouble)
        {
            rotation = rotation + new Vector3(0, 0, 90);
            //newDomino.transform.Rotate(new Vector3(0, 0, 90));

            pos = pos + new Vector2(0, shift * 3);
            //newDomino.transform.position = pos;

        }
        else if (pastDouble)
        {

            pos = pos + new Vector2(0, shift * 3);
            //newDomino.transform.position = pos;


        }
        else
        {
            pos = pos + new Vector2(0, shift * 4);
            //newDomino.transform.position = pos;

        }


        fieldData.moveCounter++;

    }
    public void MoveLeft()
    {
        //GameObject newDomino = Instantiate(prefab, new Vector2(0, 0), Quaternion.identity) as GameObject;

        if (isDouble)
        {
            rotation = rotation + new Vector3(0, 0, 180);
            //newDomino.transform.Rotate(new Vector3(0, 0, 180));
            pos.x = pos.x - shift * 3;
            // newDomino.transform.position = pos;

        }
        else if (pastDouble)
        {
            rotation = rotation + new Vector3(0, 0, 90);
            //newDomino.transform.Rotate(new Vector3(0, 0, 90));
            pos.x = pos.x - shift * 3;
            // newDomino.transform.position = pos;


        }
        else
        {
            rotation = rotation + new Vector3(0, 0, 90);
            //newDomino.transform.Rotate(new Vector3(0, 0, 90));
            pos.x = pos.x - shift * 4;
            //newDomino.transform.position = pos;

        }
        fieldData.moveCounter++;

    }
    public void MoveDown()
    {
        //GameObject newDomino = Instantiate(prefab, new Vector2(0, 0), Quaternion.identity) as GameObject;

        //newDomino.transform.Rotate(new Vector3(0, 0, 180));
        if (isDouble)
        {
            rotation = rotation + new Vector3(0, 0, 90);
            //newDomino.transform.Rotate(new Vector3(0, 0, 90));
            pos.y = pos.y - shift * 3;
            //newDomino.transform.position = pos;

        }
        else if (pastDouble)
        {
            pos.y = pos.y - shift * 3;
            //newDomino.transform.position = pos;


        }
        else
        {
            pos.y = pos.y - shift * 4;
            //newDomino.transform.position = pos;

        }

        fieldData.moveCounter++;

    }
    public void MoveRight()
    {
        //GameObject newDomino = Instantiate(prefab, new Vector2(0, 0), Quaternion.identity) as GameObject;

        if (isDouble)
        {
            rotation = rotation + new Vector3(0, 0, 0);
            //newDomino.transform.Rotate(new Vector3(0, 0, 0));
            pos.x = pos.x + shift * 3;
            //newDomino.transform.position = pos;

        }
        else if (pastDouble)
        {
            rotation = rotation + new Vector3(0, 0, 90);
            //newDomino.transform.Rotate(new Vector3(0, 0, 90));
            pos.x = pos.x + shift * 3;
            //newDomino.transform.position = pos;


        }
        else
        {
            rotation = rotation + new Vector3(0, 0, 90);
            //newDomino.transform.Rotate(new Vector3(0, 0, 90));
            pos.x = pos.x + shift * 4;
            //newDomino.transform.position = pos;

        }
        fieldData.moveCounter++;

    }

    public void turnLeft()
    {

        if (pastDouble)
        {
            pos = pos + new Vector2(-shift, 0);
        }
        else if (isDouble)
        {
            pos = pos + new Vector2(shift, shift);
        }
        else
        {
            pos = pos + new Vector2(shift, shift);


        }


    }
    public void turnDown()
    {

        if (pastDouble)
        {
            pos = pos + new Vector2(0, -shift);
        }
        else if (isDouble)
        {
            pos = pos + new Vector2(-shift, shift);
        }
        else
        {
            pos = pos + new Vector2(-shift, shift);


        }


    }
    public void turnRight()
    {
        if (pastDouble)
        {
            pos = pos + new Vector2(shift, 0);
        }
        else if (isDouble)
        {
            pos = pos + new Vector2(-shift, -shift);
        }
        else
        {
            pos = pos + new Vector2(-shift, -shift);


        }


    }
    public void turnUp()
    {

        if (pastDouble)
        {
            pos = pos + new Vector2(0, +shift);
        }
        else if (isDouble)
        {
            pos = pos + new Vector2(+shift, -shift);
        }
        else
        {
            pos = pos + new Vector2(shift, -shift);


        }


    }
}
