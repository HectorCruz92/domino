﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class handLayoutTile : MonoBehaviour{
    public Domino domino;
    public Player player;
    public fieldPlayer fieldScript;
    public bool WasPlayed=false;
    public int top;
    public int bot;
    Vector2 newPos;
    Quaternion newRot;
    float heightOnField = 0.0625f * Screen.height;
    public bool ai;
    RectTransform rt;

    private void Start()
    {
        if (!ai)
        {
            gameObject.GetComponent<Image>().sprite = domino.artwork;
        }
        
       
        top = Mathf.RoundToInt(domino.value.x);
        bot = Mathf.RoundToInt(domino.value.y);
    }
    
    // Use this for initialization


    // Update is called once per frame
    void Update () {
		

	}
    public void PlayOnField()
    {
        if (ai)
        {
            gameObject.GetComponent<Image>().sprite = domino.artwork;
        }
        var index = fieldScript.fieldData.position.Count - 1;
        newPos = fieldScript.fieldData.position[index];
        newRot = Quaternion.Euler( fieldScript.fieldData.rotation[index]);
        WasPlayed = true;
        player.removeFromHand(domino);
        rt = GetComponent<RectTransform>();
        fieldScript.gameState.NextTurn();
    }
    private void FixedUpdate()
    {
        if (WasPlayed)
        {
            rt.localPosition = Vector2.Lerp(rt.localPosition, newPos,Time.deltaTime*8);
            rt.rotation = Quaternion.Lerp(rt.rotation, newRot, Time.deltaTime * 8);
            //rt.rotation = Quaternion.Euler(Vector3.Lerp(rt.rotation.eulerAngles, newRot, Time.deltaTime * 8));
            rt.localScale = new Vector3(1, 1, 1);
            rt.sizeDelta = Vector2.Lerp(rt.sizeDelta,new Vector2(heightOnField/2,heightOnField),Time.deltaTime * 8 );
            
        }
        
    }








}
